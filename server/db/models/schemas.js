const { Schema } = require('mongoose');

const Point = {
    x: Number,
    y: Number,
};

const Bounds = {
    x: Number,
    y: Number,
    width: Number,
    height: Number,
};

const Warnings = {
    concave: Boolean,
    maxPpi: Number,
};

const Piece = {
    id: String,
    d: String,
    glass: String,
    label: String,
    labelCenter: Point,
    labelSize: Number,
    note: String,
    title: String,
    bounds: Bounds,
    area: Number,
    warnings: Warnings,
};

const Glass = {
    id: String,
    color: String,
    nightColor: String,
    title: String,
};

exports.projectSchema = Schema({
    id: String,
    owner: String,
    author: String,
    name: String,
    pieces: [Piece],
    glasses: [Glass],
    width: Number,
    height: Number,
    ppi: Number,
});

exports.user = Schema({
    id: String,
    firstName: String,
    lastName: String,
    middleName: String,
    emails: [String],
});
