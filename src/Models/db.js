// @flow
import Dexie from 'dexie';

import type { Project } from './index';

let MisterDatabase;

export const getDb = () => {
    if (MisterDatabase === undefined) {
        MisterDatabase = new Dexie('eagle');
        MisterDatabase.version(1).stores({
            projects: 'id, name',
        });
        MisterDatabase.version(2).stores({
            projects: 'id, name, owner',
            users: 'id, firstName, lastName, email',
        });
        MisterDatabase.open();
    }
    return MisterDatabase;
};

export const saveProject = async (project: Project) => {
    const db = getDb();
    db.transaction('rw', db.projects, async () => {
        db.projects.put(project);
    }).catch(err => console.error(err));
};

export const getProject = async (id: string): Promise<Project> => {
    const db = getDb();
    const project = db.projects.get(id);
    return project;
};

export const getAllProjects = async (): Promise<Array<Project>> => {
    const db = getDb();
    let projects = await db.projects.toArray();
    return projects;
};
