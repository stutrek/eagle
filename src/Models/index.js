// @flow

export type Point = {
    x: number,
    y: number,
};

export type Warnings = {
    concave: boolean,
    maxPpi: number,
};

export type Bounds = {
    x: number,
    y: number,
    width: number,
    height: number,
};

export type Glass = {
    id: string,
    color: string,
    nightColor?: string,
    title: string,
};

export type Piece = {
    id: string,
    d: string,
    glass: string | null,
    label: string,
    labelCenter: Point,
    labelSize: number,
    note: string,
    title: string,
    bounds: Bounds,
    area: number,
    warnings: Warnings,
};

export type Project = {
    id: string,
    name: string,
    pieces: Array<Piece>,
    glasses: Array<Glass>,
    width: number,
    height: number,
    ppi: number,
};
