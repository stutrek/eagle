import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import projects from 'src/reducers/projects';
import editor from 'src/Pages/Editor/reducer';
import upload from 'src/Pages/Upload/reducer';
import bootstrapStatus from 'src/reducers/bootstrap';
import keyboard from 'src/reducers/keyboard';
import auth from './reducers/auth';

export default combineReducers({
    routing: routerReducer,
    projects,
    bootstrapStatus,
    editor,
    upload,
    appConfig: () => window.appConfig,
    keyboard,
    auth,
});
