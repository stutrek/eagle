// @flow
import runtime from 'serviceworker-webpack-plugin/lib/runtime';

// import { getAllProjects } from 'src/Models/db';

export const BOOTSTRAP_START = 'BOOTSTRAP_START';
export const BOOTSTRAP_COMPLETE = 'BOOTSTRAP_COMPLETE';
export const BOOTSTRAP_FAIL = 'BOOTSTRAP_FAIL';

export default () => async (dispatch: Function) => {
    dispatch({
        type: BOOTSTRAP_START,
    });

    if (!navigator.serviceWorker) {
        dispatch({
            type: BOOTSTRAP_FAIL,
            payload: 'No serviceWorker.',
        });
        return;
    }

    const registration = await runtime.register();

    if (navigator.serviceWorker && navigator.serviceWorker.controller) {
        const request = await fetch('/api/projects');
        const projects = await request.json();

        dispatch({
            type: BOOTSTRAP_COMPLETE,
            payload: {
                projects,
            },
        });
    } else if (navigator.serviceWorker) {
        navigator.serviceWorker.addEventListener(
            'controllerchange',
            async function bootstrapListener() {
                if (navigator.serviceWorker) {
                    navigator.serviceWorker.removeEventListener(
                        'controllerchange',
                        bootstrapListener
                    );
                }
                const request = await fetch('/api/getitall');
                const projects = await request.json();

                dispatch({
                    type: BOOTSTRAP_COMPLETE,
                    payload: {
                        projects,
                    },
                });
            }
        );
    }
};
