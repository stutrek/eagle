export const ALT_DOWN = 'ALT_KEY_DOWN';
export const ALT_UP = 'ALT_KEY_UP';

export const altDown = () => ({
	type: ALT_DOWN
});

export const altUp = () => ({
	type: ALT_UP
});
