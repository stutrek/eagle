export const SHOW_LOGIN = 'SHOW_LOGIN';
export const HIDE_LOGIN = 'HIDE_LOGIN';
export const LOGIN_SUCCESSFUL = 'LOGIN_SUCCESSFUL';

export const showLogin = () => ({
    type: SHOW_LOGIN,
});

export const hideLogin = () => ({
    type: HIDE_LOGIN,
});
