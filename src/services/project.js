export async function saveProject(updatedProject) {
    const response = await fetch(`/api/project/${updatedProject.id}`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(updatedProject),
    });

    return await response.json();
}
