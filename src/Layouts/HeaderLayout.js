// @flow
import React from 'react';

import styles from './styles/layout.module.css';

export default class HeaderLayout extends React.Component<any> {
	render () {
		return <div className={styles.headerLayoutContainer}>
			{this.props.children}
		</div>;
	}
}
