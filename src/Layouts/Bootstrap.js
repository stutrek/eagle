// @flow
import React from 'react';
import { connect } from 'react-redux';

import bootstrap from 'src/actions/bootstrap';

const mapStoreToProps = store => {
    return {
        bootstrapStatus: store.bootstrapStatus,
        keyboard: store.keyboard,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        bootstrap: () => dispatch(bootstrap()),
    };
};

export default connect(
    mapStoreToProps,
    mapDispatchToProps
)(
    class LayoutWithHeader extends React.Component<any> {
        constructor(props) {
            super(props);
            this.bootstrapIfNeeded(props);
        }

        loadingRemoved = false;

        componentWillReceiveProps(props: any) {
            this.bootstrapIfNeeded(props);
        }

        bootstrapIfNeeded(props: any) {
            const { complete, inProgress } = props.bootstrapStatus;

            if (complete && !this.loadingRemoved) {
                const el = document.getElementById('loading');
                if (el && el.parentNode) {
                    el.parentNode.removeChild(el);
                }
                this.loadingRemoved;
            }

            if (complete === false && inProgress === false) {
                this.props.bootstrap();
            }
        }
        render() {
            if (this.props.bootstrapStatus.complete === false) {
                return <div />;
            }
            let className = '';
            if (this.props.keyboard.altDown) {
                className += ' alt-down';
            }
            return <div className={className}>{this.props.children}</div>;
        }
    }
);
