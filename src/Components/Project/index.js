// @flow

import React from 'react';

import Piece from './Piece';

import styles from './project.module.css';

export default class Project extends React.Component<any> {
    static defaultProps = {
        onPieceClick: () => {},
        onEyedropper: () => {},
        className: '',
        showLabels: false,
        scale: 1,
        interactive: true,
    };

    handlePieceClick = (event: SyntheticEvent<Element>) => {
        let id = event.currentTarget.getAttribute('data-id');
        this.props.onPieceClick(event, id);
    };

    render() {
        let { project, highlightGlass, nightMode } = this.props;
        let { glasses, pieces } = project;
        const nonInteractiveClassName = this.props.interactive ? '' : styles.nonInteractive;

        let g = glasses.reduce((acc, glass) => {
            acc[glass.id] = glass;
            return acc;
        }, {});

        let width = (project.width / project.ppi) * this.props.scale;
        let height = (project.height / project.ppi) * this.props.scale;

        let numberSize = project.ppi / 8;

        return (
            <svg
                id="svg"
                className={`${styles.container} ${this.props.className} ${nonInteractiveClassName}`}
                version="1.1"
                width={width + 'in'}
                height={height + 'in'}
                viewBox={`0 0 ${project.width} ${project.height}`}
                xmlns="http://www.w3.org/2000/svg"
                fill="black"
                style={this.props.styles}
            >
                {pieces.map(piece => (
                    <Piece
                        key={piece.id}
                        styles={styles}
                        piece={piece}
                        handlePieceClick={this.handlePieceClick}
                        glass={g[piece.glass]}
                        numberSize={numberSize}
                        colorOverride={this.props.colorOverride}
                        grayscale={this.props.grayscale}
                        showLabels={this.props.showLabels}
                        highlight={highlightGlass && highlightGlass === piece.glass}
                        nightMode={nightMode}
                    />
                ))}
            </svg>
        );
    }
}
