// @flow

import { saveProject } from 'src/services/project';

export const UPDATE_PROJECT = 'UPDATE_PROJECT';

import type { Piece, Glass, Project } from 'src/Models';

export interface ProjectActions {
    labelPieces(project: Project): Function;
    addGlass(project: Project, newGlass: Glass): Function;
    addGlasses(project: Project, newGlasses: Array<Glass>): Function;
    updateGlass(project: Project, updatedGlass: Glass): Function;
    deleteGlass(project: Project, updatedGlass: Glass): Function;
    changePieceGlass(project: Project, pieceId: string, updatedGlassId: ?string): Function;
}

export const labelPieces = (project: Project): Project => {
    let piecesPerGlass = project.pieces.reduce((acc, piece) => {
        if (piece.glass) {
            acc[piece.glass] = (acc[piece.glass] || 0) + 1;
        }
        return acc;
    }, {});

    let glassStartingPoints: { [string]: number } = {};

    let currentStartingNumber = 0;
    project.glasses.forEach(glass => {
        glassStartingPoints[glass.id] = currentStartingNumber;
        currentStartingNumber += piecesPerGlass[glass.id] || 0;
    });

    let currentNumberingState: { [string]: number } = {
        ...glassStartingPoints,
    };

    let pieces = project.pieces.map((piece: Piece): Piece => {
        if (piece.glass !== null) {
            let currentNumber = ++currentNumberingState[piece.glass];

            if (piece.label === currentNumber.toString()) {
                return piece;
            }

            return {
                ...piece,
                label: currentNumber.toString(),
            };
        } else {
            return {
                ...piece,
                label: '',
            };
        }
    });

    return {
        ...project,
        pieces,
    };
};

export function addGlass(project: Project, newGlass: Glass) {
    const updatedProject = {
        ...project,
        glasses: [...project.glasses, newGlass],
    };
    saveProject(updatedProject);
    return {
        type: UPDATE_PROJECT,
        payload: updatedProject,
    };
}

export function addGlasses(project: Project, newGlasses: Array<Glass>) {
    const updatedProject = {
        ...project,
        glasses: [...project.glasses, ...newGlasses],
    };
    saveProject(updatedProject);
    return {
        type: UPDATE_PROJECT,
        payload: updatedProject,
    };
}

export function updateGlass(project: Project, updatedGlass: Glass) {
    const updatedProject = {
        ...project,
        // $FlowFixMe
        glasses: project.glasses.map(g => (g.id === updatedGlass.id ? updatedGlass : g)),
    };
    saveProject(updatedProject);
    return {
        type: UPDATE_PROJECT,
        payload: updatedProject,
    };
}

export function deleteGlass(project: Project, updatedGlass: Glass) {
    const updatedProject = {
        ...project,
        // $FlowFixMe
        glasses: project.glasses.filter(g => g.id !== updatedGlass.id),
        // $FlowFixMe
        pieces: project.pieces.map(p => (p.glass !== updatedGlass.id ? p : { ...p, glass: null })),
    };
    saveProject(updatedProject);
    return {
        type: UPDATE_PROJECT,
        payload: updatedProject,
    };
}

export function changePieceGlass(project: Project, pieceId: string, updatedGlassId: ?string) {
    let updatedProject = {
        ...project,
        pieces: project.pieces.map(p => {
            if (p.id === pieceId) {
                return {
                    ...p,
                    glass: updatedGlassId,
                };
            } else {
                return p;
            }
        }),
    };
    updatedProject = labelPieces(updatedProject);
    saveProject(updatedProject);
    return {
        type: UPDATE_PROJECT,
        payload: updatedProject,
    };
}
