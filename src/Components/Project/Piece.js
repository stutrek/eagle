// @flow

import React from 'react';
import Label from './Label';
import tinycolor from 'tinycolor2';

import type { Piece, Glass } from 'src/Models';

interface PieceProps {
    piece: Piece;
    glass: Glass;
    handlePieceClick: Function;
    styles: any;
    numberSize: number;
    showLabels: boolean;
    nightMode: boolean;
    highlight: boolean;
    grayscale: ?boolean;
    colorOverride: ?string;
}

export default class PieceComponent extends React.Component<PieceProps> {
    static defaultProps = {
        showLabels: false,
        nightMode: false,
    };

    render() {
        let {
            piece,
            glass,
            handlePieceClick,
            styles,
            showLabels,
            numberSize,
            highlight,
            nightMode,
            colorOverride,
            grayscale,
        } = this.props;

        let color;
        if (!glass) {
            color = '#ffffff';
        } else if (colorOverride) {
            color = colorOverride;
        } else if (grayscale) {
            color = tinycolor(glass.color).desaturate(100);
        } else if (nightMode) {
            color = glass.nightColor || glass.color;
        } else {
            color = glass.color;
        }
        let strokeWidth = glass ? '1px' : '0px';
        let className = glass ? styles.piece : styles.emptySpace;

        if (highlight) {
            className += ` ${styles.highlighted}`;
        }

        return (
            <g>
                <path
                    className={className}
                    d={piece.d}
                    fill={color}
                    stroke="black"
                    strokeWidth={strokeWidth}
                    data-id={piece.id}
                    onClick={handlePieceClick}
                />
                {glass && showLabels ? (
                    <Label
                        piece={piece}
                        glass={glass}
                        handlePieceClick={handlePieceClick}
                        styles={styles}
                        numberSize={numberSize}
                        pieceColor={color}
                    />
                ) : null}
            </g>
        );
    }
}
