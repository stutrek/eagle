import React from 'react';

import styles from './header.module.css';

import User from './User';
import logo from '../../images/logo small text.svg';

export default class Header extends React.Component {
    static defaultProps = {
        className: '',
    };

    render() {
        return (
            <div className={styles.container + ' ' + this.props.className}>
                <a className={styles.logoContainer} href="/">
                    <span className={styles.logo} dangerouslySetInnerHTML={{ __html: logo }} />
                </a>
                <div className={styles.content}>{this.props.children}</div>
                <User {...this.props} />
            </div>
        );
    }
}
