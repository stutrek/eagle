import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as authActions from 'src/actions/auth';
import styles from './header.module.css';

const LoginDialog = props => {
    if (props.auth.dialogShowing === false) {
        return null;
    }
    return <dialog open>Login</dialog>;
};

const User = props => {
    const { user } = props.auth;
    if (user === null) {
        const listener = event => {
            event.preventDefault();
            props.authActions.showLogin();
        };
        return (
            <a className={styles.user} onClick={listener} href="/login">
                Sign In
                <LoginDialog {...props} />
            </a>
        );
    }
    return <div className={styles.user}>{user.initials}</div>;
};

export default connect(
    state => ({
        auth: state.auth,
    }),
    dispatch => ({
        authActions: bindActionCreators(authActions, dispatch),
    })
)(User);
