// @flow
import React from 'react';
import { connect } from 'react-redux';
// import { bindActionCreators } from 'redux';

import type { Project } from 'src/Models';

import styles from './glass-list.module.css';

interface PropsWithoutProject {
	projects: Array<Project>,
	params: any,
}

interface Props {
	project: Project
}


const ColorSwatch = props => {
	const { color } = props;

	return <svg width="20" height="20" viewbox="0 0 20 20">
		<rect
			x="0.5"
			y="0.5"
			width="19"
			height="19"
			fill={color}
			stroke="black"
			strokeWidth="1"
		/>
	</svg>;
};


class GlassList extends React.Component<Props> {

	render () {
		const { project } = this.props;
		const glassToPiecesMap: Map<string, string[]> = project.pieces.reduce((acc, piece) => {
			if (piece.glass === null) {
				return acc;
			}
			let existing = acc.get(piece.glass);
			if (existing === undefined) {
				acc.set(piece.glass || 'shutupflow', [piece.label]);
			} else {
				existing.push(piece.label);
			}
			return acc;
		}, new Map());
		const sortedGlasses = project.glasses.slice();
		sortedGlasses.sort((a, b) => a.title.toLowerCase() > b.title.toLowerCase() ? 1 : -1);
		return <div className={styles.container}>
			<h1>
				{project.name} Glass List <button className={styles.printButton} onClick={window.print}>Print</button>
			</h1>
			<table>
				<thead>
					<tr><td></td><td>Title</td><td>Count</td><td>Pieces</td></tr>
				</thead>
				<tbody>
					{sortedGlasses.map(glass => {
						const pieces = glassToPiecesMap.get(glass.id);
						if (!pieces) {
							return '';
						}
						let numbersString;
						if (pieces.length === 1) {
							numbersString = `${pieces[0]}`;
						} else {
							numbersString = `${pieces[0]}-${pieces[pieces.length-1]}`;
						}

						return <tr key={glass.id}>
							<td>
								<ColorSwatch color={glass.color} />
							</td>
							<td>{glass.title}</td>
							<td className={styles.count}>{pieces.length}</td>
							<td>{numbersString}</td>
						</tr>;
					})}
				</tbody>
			</table>
		</div>;
	}
}

class GlassListOrNotFound extends React.Component<PropsWithoutProject> {
	render () {
		const project = this.props.projects.find(p => p.id === this.props.params.projectId);
		if (project) {
			return <GlassList
				project={project}
			/>;
		}
		return <div>Project Not Found</div>;
	}
}
export default connect(s => s, null)(GlassListOrNotFound);
