// @flow
import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { push } from 'react-router-redux';

import HeaderLayout from 'src/Layouts/HeaderLayout';
import Header from 'src/Components/Header';

import ProgressIndicator from './Components/ProgressIndicator';

import * as uploadActions from './actions';

import ImageUpload from './Components/UploadButton';
import ImageProcessor from './Components/Processor';


export class AddUploadComponent extends React.Component<any> {
	componentWillUnmount () {
		this.props.uploadActions.clearUpload();
	}

	receiveSvg = (file: File, svg: string) => {
		this.props.uploadActions.addSvg(file, svg);
	}

	receiveImage = (file: File, dataUrl: string) => {
		this.props.uploadActions.addImage(file, dataUrl);
	}

	saveUpload = () => {
		this.props.uploadActions.saveUpload(this.props.upload);
		this.props.redirect('/');
	}

	render () {
		if (this.props.upload.dataUrl === null) {
			return <HeaderLayout>
				<Header>
					<h1>Import</h1>
					<ProgressIndicator step={1} />
				</Header>
				<ImageUpload
					receiveImage={this.receiveImage}
					receiveSvg={this.receiveSvg}
				/>
			</HeaderLayout>;

		} else {
			return <HeaderLayout>
				<Header>
					<h1>Import</h1>
					<ProgressIndicator step={2}/>
				</Header>
				<ImageProcessor
					uploadActions={this.props.uploadActions}
					upload={this.props.upload}
					saveUpload={this.saveUpload}
				/>
			</HeaderLayout>;
		}
	}
}

function mapStateToProps (store) {
	return {
		upload: store.upload,
		projects: store.projects
	};
}

function mapDispatchToProps (dispatch) {
	return {
		uploadActions: bindActionCreators(uploadActions, dispatch),
		redirect: url => dispatch(push(url))
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(AddUploadComponent);
