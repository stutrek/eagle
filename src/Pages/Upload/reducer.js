// @flow

import {
	ADD_IMAGE,
	CHANGE_IMAGE_FILTER,
	CLEAR_UPLOAD,
	//CHANGE_POTRACE_SETTING,
    ADD_PIECES
} from './actions';

import type { Piece } from 'src/Models';

export type Upload = {
	dataUrl: string | null,
	exifData: any,
	pieces: Array<Piece> | null,
	width: number | null,
	height: number | null,
	name: string,
	potraceSettings: any,
	filterSettings: {
		brightness: number,
		blur: number
	}
}

const defaultState: Upload = {
	dataUrl: null,
	exifData: {},
	pieces: null,
	width: null,
	height: null,
	name: '',
	potraceSettings: {
		turdSize: 10
	},
	filterSettings: {
		brightness: 100,
		blur: 0
	}
};

function handleAddImage (state: Upload, action: any) {

	return {
		...state,
		...action.payload,
	};
}

export default function (state: Upload=defaultState, action: any) {
	switch (action.type) {
		case CLEAR_UPLOAD:
			state = defaultState;
			break;

		case ADD_IMAGE:
			state = handleAddImage(state, action);
			break;

		case CHANGE_IMAGE_FILTER:
			state = {
				...state,
				filterSettings: {
					...state.filterSettings,
					[action.payload.setting]: action.payload.value
				}
			};
			break;
		case ADD_PIECES:
			state = {
				...state,
				pieces: action.payload
			};
	}

	return state;
}
