// @flow

import createPieces from 'src/imageProcessing/createPieces';
import coloredSvgToWhite from 'src/imageProcessing/coloredSVGtoWhite';
import { labelPieces } from 'src/Components/Project/actions';

import exif from 'exif-js';
import uuid from 'uuid';

import uniq from 'lodash/uniq';

import { saveProject as saveProjectToDb } from 'src/Models/db';

export const ADD_IMAGE = 'UPLOAD ADD_IMAGE';
export const ADD_PIECES = 'UPLOAD ADD_PIECES';
export const CHANGE_IMAGE_FILTER = 'UPLOAD CHANGE_IMAGE_FILTER';
export const CHANGE_POTRACE_SETTING = 'UPLOAD CHANGE_POTRACE_SETTING';
export const ADD_PROJECT = 'UPLOAD ADD_PROJECT';
export const CLEAR_UPLOAD = 'UPLOAD CLEAR_NEW_PROJECT';

import type { Project } from 'src/Models';
// import type { Upload } from './reducer';

export function clearUpload () {
	return {
		type: CLEAR_UPLOAD
	};
}

export const addSvg = (file: File, svgString: string) => (dispatch: Function) => {
	// const div = document.createElement('div');
	// div.innerHTML = svgString;
	var parser = new DOMParser();
	var doc = parser.parseFromString(svgString, 'image/svg+xml');

	const dataUrl = `data:image/svg+xml,${escape(svgString)}`;

	const width = doc.documentElement.width.baseVal.value;
	const height = doc.documentElement.height.baseVal.value;

	dispatch({
		type: ADD_IMAGE,
		payload: {
			dataUrl,
			svgString,
			type: file.type,
			ppi: 72,
			scale: 4,
			name: file.name.replace(/\.\w+$/, ''),
			width,
			height
		}
	});
};

export const addImage = (file: File, dataUrl: string) => (dispatch: Function) => {
	let img = new Image();
	img.onload = () => {
		exif.getData(img, function () {
			let exifData = exif.getAllTags(this);
			const ppi = exifData.XResolution || 72;
			const scale = ppi / 72;

			dispatch({
				type: ADD_IMAGE,
				payload: {
					dataUrl,
					type: file.type,
					svgString: null,
					ppi: exifData.XResolution || 72,
					scale,
					name: file.name.replace(/\.\w+$/, ''),
					width: img.naturalWidth / scale,
					height: img.naturalHeight / scale
				}
			});
		});
	};
	img.src = dataUrl;
};

export const changeFilterSetting = (setting: string, value: any) => {
	return {
		type: CHANGE_IMAGE_FILTER,
		payload: {
			setting,
			value
		}
	};
};

const preprocessUpload = (upload) => {
	const { scale, width, height, filterSettings, svgString } = upload;
	let dataUrl;
	if (upload.type === 'image/svg+xml') {
		dataUrl = `data:image/svg+xml,${escape(coloredSvgToWhite(svgString).outerHTML)}`;
	} else {
		dataUrl = upload.dataUrl;
	}

	return new Promise(async (resolve) => {
		const image = new Image();
		const canvas = document.createElement('canvas');

		image.width = width * scale;
		image.height = height * scale;
		image.src = dataUrl;

		canvas.width = width * scale;
		canvas.height = height * scale;
		let ctx = canvas.getContext('2d');

		image.onload = async () => {

			let {brightness, blur} = filterSettings;

			ctx.fillStyle = 'white';
			ctx.fillRect(0, 0, canvas.width, canvas.height);
			if (brightness >= 100) {
				ctx.filter = `blur(${blur}px) contrast(500%) brightness(${brightness}%)`;
				ctx.drawImage(image, 0, 0, canvas.width, canvas.height);
			} else {
				ctx.filter = `blur(${blur}px) contrast(500%)`;
				ctx.drawImage(image, 0, 0, canvas.width, canvas.height);
				ctx.globalCompositeOperation = 'multiply';
				let counter = 100 - brightness;
				while (counter >= 10) {
					ctx.drawImage(image, 0, 0, canvas.width, canvas.height);
					counter -= 10;
				}
				ctx.globalAlpha = counter / 10;
				ctx.drawImage(image, 0, 0, canvas.width, canvas.height);
			}
			resolve(canvas);
		};
	});
};

const getColorImage = (upload) => {
	const { scale, width, height } = upload;
	return new Promise(resolve => {
		if (upload.type === 'image/svg+xml') {
			const colorImage = new Image();
			colorImage.height = height * scale;
			colorImage.width = width * scale;
			colorImage.src = upload.dataUrl;

			colorImage.onload = () => {
				resolve(colorImage);
			};
		} else {
			resolve(null);
		}
	});
};

export const createShapesFromCanvas = (upload: any) => async (dispatch: Function) => {

	const canvas = await preprocessUpload(upload);
	const colorImage = await getColorImage(upload);

	let pieces = await createPieces(canvas, colorImage, upload);

	dispatch({
		type: ADD_PIECES,
		payload: pieces
	});
};

export const saveUpload = (upload: any) => {

	const glassColors = uniq(upload.pieces.map(p => p.defaultColor));
	const glasses = glassColors.map((rgb, i) => {
		if (rgb === null) {
			return {
				id: uuid(),
				title: 'transparent',
				color: '#ffffff'
			};
		}
		return {
			id: uuid(),
			title: `Color ${i+1}`,
			color: rgb
		};
	});

	const hexToId = {};
	glasses.forEach(glass => {
		hexToId[glass.color] = glass.id;
	});

	const pieces = upload.pieces.map(piece => {
		return {
			...piece,
			glass: hexToId[piece.defaultColor] || null
		};
	});

	let project: Project = labelPieces({
		id: uuid(),
		name: upload.name,
		pieces,
		width: upload.width,
		height: upload.height,
		ppi: upload.ppi,
		glasses
	});
	saveProjectToDb(project);

	return {
		type: ADD_PROJECT,
		payload: project
	};
};
