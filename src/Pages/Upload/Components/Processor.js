// @flow
import React from 'react';

import Sidebar from 'src/Components/Sidebar';

import SVGPreview from './SVGPreview';
import PreprocessorOptions from './PreprocessorOptions';

import styles from '../add-project.module.css';
import button from 'src/styles/button.module.css';

const roundNumber = val => val.toFixed(2).replace(/\.0+$/, '');

interface ProcessorProps {
	upload: any,
	uploadActions: any
}

interface State {
	colorMethod: string
}

export default class ImageProcessor extends React.Component<ProcessorProps, State> {

	image: ?HTMLImageElement = null
	canvas: ?HTMLCanvasElement = null

	state = {
		colorMethod: this.props.upload.type === 'image/svg+xml' ? 'source' : 'white'
	}

	componentDidUpdate (prevProps: ProcessorProps) {
		let prevUpload = prevProps.upload;
		let nextUpload = this.props.upload;
		let canvasNeedsUpdate = prevUpload.filterSettings !== nextUpload.filterSettings || prevUpload.dataUrl !== nextUpload.dataUrl;

		if (canvasNeedsUpdate) {
			this.createShapes();
		}
	}

	componentDidMount () {
		this.createShapes();
	}

	createShapes = () => {
		this.props.uploadActions.createShapesFromCanvas(
			this.props.upload
		);
	}

	handleFilterInputChange = (event: SyntheticEvent<HTMLInputElement>) => {
		this.props.uploadActions.changeFilterSetting(event.currentTarget.name, parseFloat(event.currentTarget.value));
	}

	changeColorMethod = (event: SyntheticEvent<HTMLInputElement>) => this.setState({colorMethod: event.currentTarget.value})

	render () {
		const project = this.props.upload;
		return <div className={styles.container}>
			<Sidebar>
				<h3>{project.name}</h3>
				<div className={styles.dimensions}>
					{roundNumber(project.width / project.ppi)}" &times; {roundNumber(project.height / project.ppi)}"
				</div>
				<hr />
				<PreprocessorOptions
					project={project}
					handleChange={this.handleFilterInputChange}
				/>
				<hr />
				<div>
					Color method:
					<label>
						<input
							type="radio"
							value="location"
							checked={this.state.colorMethod === 'location'}
							onChange={this.changeColorMethod}
						/> Location
					</label>
					<label>
						<input
							type="radio"
							value="random"
							checked={this.state.colorMethod === 'random'}
							onChange={this.changeColorMethod}
						/> Random
					</label>
					<label>
						<input
							type="radio"
							value="white"
							checked={this.state.colorMethod === 'white'}
							onChange={this.changeColorMethod}
						/> White
					</label>
					{ project.type === 'image/svg+xml' ? <label>
						<input
							type="radio"
							value="source"
							checked={this.state.colorMethod === 'source'}
							onChange={this.changeColorMethod}
						/> Source
					</label> : null }
					<hr />
					<button onClick={this.props.saveUpload} className={button.block}>
						Save
					</button>
				</div>
			</Sidebar>
			<SVGPreview
				project={project}
				colorMethod={this.state.colorMethod}
			/>
		</div>;
	}
}
