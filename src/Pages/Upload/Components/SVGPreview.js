// @flow
import React from 'react';

import tinycolor from 'tinycolor2';

import type { Upload, Piece } from 'src/Models';

import styles from '../add-project.module.css';

interface Props {
	project: Upload,
	context: CanvasRenderingContext2D
}

interface State {
	canvas: HTMLCanvasElement | null
}


export default class SVGPreview extends React.Component<Props, State> {

	state: State = {
		canvas: null
	}

	getColor (piece: Piece, project: Upload): string {
		if (this.props.colorMethod === 'location') {
			return tinycolor({r: (piece.bounds.x / project.width) * 255, g: (piece.bounds.y / project.height) * 255, b:0});
		}

		if (this.props.colorMethod === 'random') {
			return tinycolor.random();
		}

		if (this.props.colorMethod === 'source' ) {
			return piece.defaultColor;
		}

		return '#fff';
	}

	getWarningClass (piece: Piece) {
		let classes = [];
		if (piece.warnings.concave) {
			classes.push(styles.concaveWarning);
		}
		if (piece.warnings.maxPpi < this.props.project.ppi) {
			classes.push(styles.sizeWarning);
		}
		return classes.join(' ');
	}

	render () {
		const { project } = this.props;
		if (!project.pieces || project.pieces.length === 0) {
			return <div className={styles.svgPreview}>Processing...</div>;
		}
		return <div className={styles.svgPreview}>
			<svg id="svg"
				version="1.1"
				viewBox={`0 0 ${project.width} ${project.height}`}
				xmlns="http://www.w3.org/2000/svg"
				fill="black"
				width={project.width + 'px'}
				height={project.height + 'px'}
			>
				{project.pieces ? project.pieces.map(piece => <path
					key={piece.id}
					d={piece.d}
					fill={this.getColor(piece, project)}
					stroke="black"
					className={this.getWarningClass(piece)}
				/>) : null}
			</svg>
		</div>;
	}
}
