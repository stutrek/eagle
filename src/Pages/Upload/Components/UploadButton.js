// @flow
import React from 'react';

import styles from './uploadButton.module.css';

interface Props {
	receiveImage: Function,
	className: ?string,
	children: any
}

export default class ImageUploader extends React.Component<Props> {

	static defaultProps = {
		className: ''
	}

	handleDragOver = event => {
		event.preventDefault();
		event.dataTransfer.dropEffect = 'copy';
	}

	receiveDrop = (event) => {
		event.preventDefault();
		this.processFile(event.dataTransfer.files[0]);
	}

	receiveImage = (event: SyntheticEvent<HTMLInputElement>) => {
		const { files } = event.currentTarget;
		if (files[0]) {
			this.processFile(files[0]);
		}
	}

	processFile = (file) => {
		let fileReader = new FileReader();
		if (file.type.includes('svg')) {
			fileReader.onload = (e) => {
				this.props.receiveSvg(file, e.target.result);
			};
			fileReader.readAsText(file);
		} else {
			fileReader.onload = (e) => {
				this.props.receiveImage(file, e.target.result);
			};
			fileReader.readAsDataURL(file);
		}

	}

	render () {
		return <label
			className={styles.upload}
			onDrop={this.receiveDrop}
			onDragOver={this.handleDragOver}
		>
			<input type="file" onChange={this.receiveImage} />
			<div className={styles.text}>
				Click to select an image<br/>
				or drag a file here.
			</div>
		</label>;
	}
}
