//@flow
import React from 'react';

import styles from './progress-indicator.module.css';

export default ({step}) => {
	const getClass = n => {
		switch(true) {
			case n === step:
				return styles.active;
			case n < step:
				return styles.complete;
		}
		return styles.step;
	};

	return <div className={styles.container}>
		<div className={getClass(1)}>
			<div className={styles.text}>1. Upload SVG or Outlines</div>
		</div>
		<div className={getClass(2)}>
			<div className={styles.text}>2. Verify pieces are correct</div>
		</div>
		<div className={getClass(3)}>
			<div className={styles.text}>
				3. Save
			</div>
		</div>
	</div>;
};
