import React from 'react';

import styles from '../add-project.module.css';

const SvgOptions = ({project, handleChange}) => <div className={styles.preprocessorSettings}>
	<label>
		Soften
		<input
			type="range"
			name="blur"
			min="0"
			max="2"
			value={project.filterSettings.blur}
			onChange={handleChange}
			step="0.01"
		/>
	</label>
</div>;

const RasterOptions = ({project, handleChange}) => <div className={styles.preprocessorSettings}>
	<label>
		Brightness
		<input
			type="range"
			name="brightness"
			min="0"
			max="200"
			value={project.filterSettings.brightness}
			onChange={handleChange}
		/>
	</label>
	<br />
	<label>
		Blur
		<input
			type="range"
			name="blur"
			min="0"
			max="2"
			value={project.filterSettings.blur}
			onChange={handleChange}
			step="0.01"
		/>
	</label>
</div>;

export default ({project, handleChange}) => {
	if (project.type === 'image/svg+xml') {
		return <SvgOptions
			project={project}
			handleChange={handleChange}
		/>;
	} else {
		return <RasterOptions
			project={project}
			handleChange={handleChange}
		/>;
	}
};