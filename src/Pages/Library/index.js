import React from 'react';
import { connect } from 'react-redux';

import HeaderLayout from 'src/Layouts/HeaderLayout';
import Header from 'src/Components/Header';

import Project from 'src/Components/Project';

import styles from './library.module.css';

class HomePage extends React.Component {
    render() {
        return (
            <HeaderLayout>
                <Header>
                    <h1>Library</h1>
                </Header>
                <div className={styles.container}>
                    <div className={styles.addItem}>
                        <a href="/add">+ Import Design</a>
                    </div>
                    {this.props.projects &&
                        this.props.projects
                            .map(project => (
                                <a
                                    className={styles.libraryItem}
                                    key={project.id}
                                    href={`/edit/${project.id}`}
                                >
                                    <Project
                                        className={styles.project}
                                        project={project}
                                        interactive={false}
                                    />
                                    {project.name}
                                </a>
                            ))
                            .reverse()}
                </div>
            </HeaderLayout>
        );
    }
}

export default connect(
    s => s,
    null
)(HomePage);
