// @flow
import React from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as editorActions from './actions';
import * as projectActions from 'src/Components/Project/actions';

import HeaderLayout from 'src/Layouts/HeaderLayout';
import Header from 'src/Components/Header';

import Toolbar from './Components/Toolbar';
import Sidebar from './Components/Sidebar';
import ProjectComponent from 'src/Components/Project';

import type { Project } from 'src/Models';
import { EditorState } from './reducer';
import type { EditorActions } from './actions';
import type { ProjectActions } from 'src/Components/Project/actions';

import styles from './editor.module.css';

interface PropsWithoutProject {
    projects: Array<Project>;
    params: any;
    editor: EditorState;
    projectActions: ProjectActions;
    editorActions: EditorActions;
}

interface Props {
    projects: Array<Project>;
    params: any;
    editor: EditorState;
    projectActions: ProjectActions;
    editorActions: EditorActions;
    project: Project;
}

interface DragStart {
    x: number;
    y: number;
    initialX: number;
    initialY: number;
}

interface State {
    zoom: number;
    minZoom: number;
    margin: number;
    contentWidth: number;
    containerHeight: number;
    contentHeight: number;
    sizeCalculated: boolean;
    containerWidth: number;
}

class Editor extends React.Component<Props, State> {
    projectContainer: HTMLDivElement;
    projectWrapper: HTMLDivElement;

    dragStart: ?DragStart;
    isDragging: boolean = false;
    grabCursorTimeout: ?TimeoutID;

    state = {
        zoom: 1,
        minZoom: 1,
        containerWidth: -1,
        containerHeight: -1,
        margin: -1,
        contentWidth: (this.props.project.width / this.props.project.ppi) * 96,
        contentHeight: (this.props.project.height / this.props.project.ppi) * 96,
        sizeCalculated: false,
    };

    componentDidMount() {
        const containerWidth = this.projectContainer.offsetWidth;
        const containerHeight = this.projectContainer.offsetHeight;
        this.setState({
            containerWidth,
            containerHeight,
            margin: containerWidth / 2,
        });

        this.setZoomToContent(containerWidth, containerHeight, containerWidth / 2);
    }

    setZoomToContent = (containerWidth, containerHeight, margin) => {
        const container = this.projectContainer;

        const scale = Math.min(
            containerWidth / this.state.contentWidth,
            containerHeight / this.state.contentHeight
        );

        this.setState(
            {
                zoom: scale,
                minZoom: scale,
                sizeCalculated: true,
            },
            () => {
                const updatedWorkspaceWidth =
                    this.state.contentWidth * this.state.zoom + margin * 2;
                const updatedWorkspaceHeight =
                    this.state.contentHeight * this.state.zoom + margin * 2;

                const overflowWidth = updatedWorkspaceWidth - containerWidth;
                const overflowHeight = updatedWorkspaceHeight - containerHeight;

                container.scrollLeft = overflowWidth / 2;
                container.scrollTop = overflowHeight / 2;
            }
        );
    };

    setProjectContainer = (el: HTMLDivElement | null) => {
        if (el) {
            this.projectContainer = el;
        }
    };

    getSvgString = colorMode => {
        const { project } = this.props;

        const fixture = document.createElement('div');
        const renderedProject = ReactDOM.render(
            <ProjectComponent
                scale={1}
                project={project}
                showLabels={true}
                colorOverride={colorMode === 'white' && 'white'}
                grayscale={colorMode === 'grayscale'}
            />,
            fixture
        );
        const svgString = fixture.innerHTML;

        return svgString;
    };

    openInNewWindow = () => {
        let newWindow = window.open();
        newWindow.document.open().write(this.getSvgString());
        newWindow.document.title = this.props.project.name;
    };

    getDownloadHref = colorMode => {
        return `data:image/svg+xml;utf-8,${escape(this.getSvgString(colorMode))}`;
    };

    handlePieceClick = (event, id: string) => {
        if (this.isDragging) {
            return;
        }
        if (this.grabCursorTimeout) {
            clearTimeout(this.grabCursorTimeout);
        }
        if (event.altKey) {
            this.handleEyedropper(id);
            return;
        }
        if (this.props.editor.mode === 'change-glass') {
            this.props.projectActions.changePieceGlass(
                this.props.project,
                id,
                this.props.editor.modeModifier
            );
        }
    };

    handleEyedropper = (id: string) => {
        const project = this.props.project;
        const piece = project.pieces.find(p => p.id === id);
        if (!piece) {
            return;
        }
        const glass = project.glasses.find(g => g.id === piece.glass);
        this.props.editorActions.changeMode('change-glass', glass ? glass.id : null);
    };

    startPotentialDrag = (event: SyntheticMouseEvent<Element>) => {
        this.dragStart = {
            x: event.pageX,
            y: event.pageY,
            initialX: this.projectContainer.scrollLeft,
            initialY: this.projectContainer.scrollTop,
        };
        this.grabCursorTimeout = setTimeout(() => {
            this.isDragging = true;
            this.projectContainer.style.cursor = 'grab';
        }, 750);
    };

    initiateDrag = (event: SyntheticMouseEvent<Element>) => {
        const dragStart = this.dragStart;
        if (dragStart) {
            let distance = Math.max(
                Math.abs(event.pageX - dragStart.x),
                Math.abs(event.pageY - dragStart.y)
            );
            if (distance < 10) {
                return;
            }
            if (this.grabCursorTimeout) {
                clearTimeout(this.grabCursorTimeout);
            }
            this.isDragging = true;
            this.projectContainer.style.cursor = 'grab';
            this.handleDrag(event);
            window.addEventListener('mousemove', this.handleDrag);
            window.addEventListener('mouseup', this.endDrag);
        }
    };

    handleDrag = (event: SyntheticMouseEvent<Element>) => {
        if (!this.dragStart) {
            return;
        }
        if (this.state.zoom === this.state.minZoom) {
            return;
        }
        this.projectContainer.scrollLeft = this.dragStart.initialX + this.dragStart.x - event.pageX;
        this.projectContainer.scrollTop = this.dragStart.initialY + this.dragStart.y - event.pageY;

        event.preventDefault();
    };

    endDrag = () => {
        // let the click handler happen first
        window.removeEventListener('mousemove', this.handleDrag);
        window.removeEventListener('mouseup', this.endDrag);
        setTimeout(() => {
            this.dragStart = undefined;
            this.isDragging = false;
            this.projectContainer.style.cursor = '';
        }, 4);
    };

    setZoom = (newLevel: number) => {
        newLevel = Math.min(newLevel, 3);
        newLevel = Math.max(newLevel, this.state.minZoom);

        if (newLevel === this.state.minZoom) {
            this.setZoomToContent(
                this.state.containerWidth,
                this.state.containerHeight,
                this.state.margin
            );
        } else if (newLevel !== this.state.zoom) {
            let diff = this.state.zoom - newLevel;
            let scrollScale = 1 - diff / 4;
            this.setState(
                {
                    zoom: newLevel,
                },
                () => {
                    this.projectContainer.scrollTop = this.projectContainer.scrollTop * scrollScale;
                    this.projectContainer.scrollLeft =
                        this.projectContainer.scrollLeft * scrollScale;
                }
            );
        }
    };

    render() {
        let project = this.props.project;
        return (
            <HeaderLayout>
                <Header>
                    <Toolbar
                        project={project}
                        getDownloadHref={this.getDownloadHref}
                        openInNewWindow={this.openInNewWindow}
                        setNightMode={this.props.editorActions.setNightMode}
                        setLabelsShowing={this.props.editorActions.setLabelsShowing}
                        setZoom={this.setZoom}
                        currentZoom={this.state.zoom}
                        minZoom={this.state.minZoom}
                        maxZoom={3}
                        labelsShowing={this.props.editor.labelsShowing}
                        nightMode={this.props.editor.nightMode}
                    />
                </Header>
                <div className={styles.layout}>
                    <Sidebar {...this.props} />
                    <div
                        className={styles.project}
                        ref={this.setProjectContainer}
                        onMouseDown={this.startPotentialDrag}
                        onMouseMove={this.initiateDrag}
                        onMouseUp={this.endDrag}
                        style={{
                            overflow: this.state.zoom === this.state.minZoom ? 'hidden' : 'auto',
                        }}
                    >
                        {this.state.sizeCalculated ? (
                            <ProjectComponent
                                scale={this.state.zoom}
                                styles={{ margin: this.state.margin }}
                                project={project}
                                onPieceClick={this.handlePieceClick}
                                showLabels={this.props.editor.labelsShowing}
                                onEyedropper={this.handleEyedropper}
                                highlightGlass={this.props.editor.highlightGlass}
                                nightMode={this.props.editor.nightMode}
                            />
                        ) : null}
                    </div>
                </div>
            </HeaderLayout>
        );
    }
}

class EditorOrNotFound extends React.Component<PropsWithoutProject> {
    render() {
        const project = this.props.projects.find(p => p.id === this.props.params.projectId);
        if (project) {
            return <Editor {...this.props} project={project} key={project.id} />;
        }
        return <div>Project Not Found</div>;
    }
}

const mapDispatchToProps = dispatch => ({
    editorActions: bindActionCreators(editorActions, dispatch),
    projectActions: bindActionCreators(projectActions, dispatch),
});

export default connect(
    s => s,
    mapDispatchToProps
)(EditorOrNotFound);
