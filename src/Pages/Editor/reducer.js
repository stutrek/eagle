// @flow
import type { Piece } from 'src/Models';

import { CHANGE_MODE, HIGHLIGHT_GLASS, SET_NIGHT_MODE, SET_LABELS_SHOWING } from './actions';
import { Action } from 'redux';

export interface EditorState {
    mode: string;
    modeModifier: ?string;
    selection: Array<Piece>;
    highlightGlass: ?string;
    nightMode: boolean;
    labelsShowing: boolean;
}

const initialState: EditorState = {
    mode: 'select',
    modeModifier: undefined,
    selection: [],
    highlightGlass: undefined,
    nightMode: false,
    labelsShowing: true,
};

export default function(state: EditorState = initialState, action: Action) {
    switch (action.type) {
        case CHANGE_MODE:
            state = {
                ...state,
                ...action.payload,
            };
            break;

        case HIGHLIGHT_GLASS:
            state = {
                ...state,
                highlightGlass: action.payload.glassId,
            };
            break;

        case SET_NIGHT_MODE:
            state = {
                ...state,
                nightMode: action.payload,
            };
            break;

        case SET_LABELS_SHOWING:
            state = {
                ...state,
                labelsShowing: action.payload,
            };
    }
    return state;
}
