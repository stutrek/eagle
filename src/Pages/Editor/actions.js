// @flow

export const CHANGE_MODE = 'CHANGE_MODE';
export const HIGHLIGHT_GLASS = 'HIGHLIGHT_GLASS';
export const SET_NIGHT_MODE = 'SET_NIGHT_MODE';
export const SET_LABELS_SHOWING = 'SET_LABELS_SHOWING';

export interface EditorActions {
    changeMode(mode: string, modeModifier: ?string): Function;
    highlightGlass(glassId: ?string): any;
    setNightMode(newSetting: boolean): any;
    setLabelsShowing(newSetting: boolean): any;
}

export function changeMode(mode: string, modeModifier: ?string): any {
    return {
        type: CHANGE_MODE,
        payload: {
            mode,
            modeModifier,
        },
    };
}

export function highlightGlass(glassId: ?string): any {
    return {
        type: HIGHLIGHT_GLASS,
        payload: {
            glassId,
        },
    };
}

export function setNightMode(newSetting: boolean): any {
    return {
        type: SET_NIGHT_MODE,
        payload: newSetting,
    };
}

export function setLabelsShowing(newSetting: boolean): any {
    return {
        type: SET_LABELS_SHOWING,
        payload: newSetting,
    };
}
