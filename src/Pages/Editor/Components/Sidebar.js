//@flow
import React from 'react';
import uuid from 'uuid';


import GlassSelector from './Glass';
import NoneGlassSelector from './NoneGlass';

import ImportGlassDialog from './ImportGlass';

import styles from '../editor.module.css';

import type { Glass, Project } from 'src/Models';

import { EditorState } from '../reducer';
import type { EditorActions } from '../actions';

import type { ProjectActions } from 'src/Components/Project/actions';


interface Props {
	editorActions: EditorActions,
	projectActions: ProjectActions,
	editor: EditorState,
	project: Project,
	projects: Array<Project>
}

type State = {
	importGlassOpen: boolean
}


export default class Sidebar extends React.Component<Props, State> {
	state = {
		importGlassOpen: false
	}

	addGlass = () => {
		let project = this.props.project;
		const glass: Glass = {
			id: uuid(),
			color: '#999999',
			title: 'New Glass'
		};
		this.props.projectActions.addGlass(project, glass);
		this.props.editorActions.changeMode('change-glass', glass.id);
	}

	closeImportGlassDialog = () => {
		this.setState({
			importGlassOpen: false
		});
	}

	openImportGlassDialog = () => {
		this.setState({
			importGlassOpen: true
		});
	}

	render () {
		const { project } = this.props;
		return <div className={styles.sidebar}>
			{/* <button>Undo</button>
			<button>Redo</button> */}
			<table className={styles.glassTable}>
				<thead>
					<tr>
						<td className={styles.center}>Day</td>
						<td className={styles.center}>Night</td>
						<td>Title</td>
						<td />
					</tr>
				</thead>
				<tbody>
					<NoneGlassSelector
						glass={null}
						editor={this.props.editor}
						editorActions={this.props.editorActions}
					/>
					{project.glasses.map(glass => <GlassSelector
						key={glass.id}
						glass={glass}
						project={project}
						editor={this.props.editor}
						editorActions={this.props.editorActions}
						projectActions={this.props.projectActions}
					/>)}
				</tbody>
			</table>
			<div onClick={this.addGlass} className={styles.sidebarButton}>
				+ New Glass
			</div>
			<div onClick={this.openImportGlassDialog} className={styles.sidebarButton}>
				Import Glass from another project
			</div>
			{this.state.importGlassOpen ? <ImportGlassDialog
				project={this.props.project}
				projects={this.props.projects}
				projectActions={this.props.projectActions}
				closeDialog={this.closeImportGlassDialog}
			/> : null}
		</div>;
	}
}
