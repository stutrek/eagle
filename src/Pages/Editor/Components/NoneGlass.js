// @flow

import React from 'react';
import type { EditorState } from '../reducer';
import type { EditorActions } from '../actions';
import styles from '../editor.module.css';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { faTimesCircle } from '@fortawesome/free-regular-svg-icons';

interface Props {
    editorActions: EditorActions;
    editor: EditorState;
}

export default class GlassSelector extends React.Component<Props> {
    claimMode = () => {
        this.props.editorActions.changeMode('change-glass', null);
    };

    render() {
        let { mode, modeModifier } = this.props.editor;

        let isSelected = mode === 'change-glass' && modeModifier === undefined;
        let className = isSelected ? styles.glassSelected : styles.glassNotSelected;

        return (
            <tr className={className} onClick={this.claimMode}>
                <td>
                    <span className={styles.icon}>
                        <FontAwesomeIcon icon={faTimesCircle} />
                    </span>
                </td>
                <td>
                    <span className={styles.icon}>
                        <FontAwesomeIcon icon={faTimesCircle} />
                    </span>
                </td>
                <td>
                    <div className={styles.title}>Empty Space</div>
                </td>
                <td />
            </tr>
        );
    }
}
