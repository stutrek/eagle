// @flow

import React from 'react';
import { Dropdown, Menu, Icon } from 'semantic-ui-react';

import type { Glass, Project } from 'src/Models';
import type { EditorState } from '../reducer';
import type { EditorActions } from '../actions';
import type { ProjectActions } from 'src/Components/Project/actions';

import styles from '../editor.module.css';

interface Props {
    glass: Glass;
    project: Project;
    editorActions: EditorActions;
    projectActions: ProjectActions;
    editor: EditorState;
}

interface MenuItem {
    key: string;
    text: string;
    onClick: Function;
    icon: ?string;
}

export default class GlassSelector extends React.Component<Props> {
    moreOptionsMenu: MenuItem[] = [
        {
            key: 'delete',
            text: 'Delete',
            onClick: () => {
                if (confirm(`Are you sure you want to delete ${this.props.glass.title}?`)) {
                    this.props.projectActions.deleteGlass(this.props.project, this.props.glass);
                }
            },
            icon: undefined,
        },
    ];

    claimMode = () => {
        let { glass } = this.props;
        this.props.editorActions.changeMode('change-glass', glass.id);
    };

    handleChange = (event: SyntheticEvent<HTMLInputElement>) => {
        let newGlass = {
            ...this.props.glass,
            [event.currentTarget.name]: event.currentTarget.value,
        };
        this.props.projectActions.updateGlass(this.props.project, newGlass);
    };

    handleTextareaFocus = (event: SyntheticEvent<HTMLTextAreaElement>) => {
        event.currentTarget.select();
        this.props.editorActions.highlightGlass(this.props.glass.id);
    };

    handleTextareaBlur = () => {
        this.props.editorActions.highlightGlass(undefined);
    };

    // handleMoreOptions = (event: SyntheticEvent<HTMLInputElement>, { callback }: MenuItem) => {
    //     callback();
    // };

    render() {
        const { glass } = this.props;
        let { mode, modeModifier } = this.props.editor;

        let isSelected = mode === 'change-glass' && modeModifier === glass.id;
        let className = isSelected ? styles.glassSelected : styles.glassNotSelected;

        return (
            <tr className={className}>
                <td onClick={this.claimMode} className={styles.colorCell}>
                    <label className={styles.color} style={{ backgroundColor: glass.color }}>
                        <input
                            type="color"
                            name="color"
                            value={glass.color}
                            onChange={this.handleChange}
                            tabIndex={-1}
                        />
                    </label>
                </td>
                <td onClick={this.claimMode} className={styles.colorCell}>
                    <label
                        className={glass.nightColor ? styles.color : styles.noColor}
                        style={{ backgroundColor: glass.nightColor || glass.color }}
                    >
                        <input
                            type="color"
                            name="nightColor"
                            value={glass.nightColor || glass.color}
                            onChange={this.handleChange}
                            tabIndex={-1}
                        />
                    </label>
                </td>
                <td onClick={this.claimMode}>
                    <textarea
                        className={styles.title}
                        name="title"
                        value={glass.title}
                        rows={2}
                        onChange={this.handleChange}
                        onFocus={this.handleTextareaFocus}
                        onBlur={this.handleTextareaBlur}
                    />
                </td>
                <td onClick={this.claimMode}>
                    <Dropdown
                        trigger={<Icon circular name="ellipsis horizontal" size="small" />}
                        icon={null}
                        options={this.moreOptionsMenu}
                        pointing="top right"
                    />
                </td>
            </tr>
        );
    }
}
