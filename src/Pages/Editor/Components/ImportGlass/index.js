//@flow
import React from 'react';
import uuid from 'uuid';

import styles from './import-glass.module.css';

import type { Project } from 'src/Models';

type Props = {
    projects: Array<Project>,
    project: Project,
    projectActions: any,
    closeDialog: Function,
};

type State = {
    selectedId: ?string,
};
export default class ImportGlass extends React.Component<Props, State> {
    state = {
        selectedId: null,
    };

    import = () => {
        const selectedProject = this.props.projects.find(p => p.id === this.state.selectedId);
        if (!selectedProject) {
            return;
        }
        const updatedGlasses = selectedProject.glasses.map(g => ({ ...g, id: uuid() }));
        this.props.projectActions.addGlasses(this.props.project, updatedGlasses);
        this.props.closeDialog();
    };

    selectProject = (event: SyntheticEvent<HTMLElement>) => {
        this.setState({
            selectedId: event.currentTarget.dataset.projectId,
        });
    };

    showModal = (el: ?HTMLElement) => {
        if (el instanceof HTMLDialogElement) {
            el.showModal();
        }
    };

    render() {
        const selectedProject = this.props.projects.find(p => p.id === this.state.selectedId);
        return (
            <dialog ref={this.showModal} className={styles.container}>
                <div className={styles.projects}>
                    {this.props.projects.map(project => (
                        <div
                            className={styles.project}
                            key={project.id}
                            onClick={this.selectProject}
                            data-project-id={project.id}
                        >
                            {project.name}
                        </div>
                    ))}
                </div>
                <div className={styles.glasses}>
                    {selectedProject &&
                        selectedProject.glasses.map(glass => (
                            <div className={styles.glass} key={glass.id}>
                                <div
                                    className={styles.colorSwatch}
                                    style={{ backgroundColor: glass.color }}
                                />
                            </div>
                        ))}
                </div>
                <div className={styles.buttons}>
                    <button onClick={this.props.closeDialog}>Cancel</button>
                    <button onClick={this.import}>Import</button>
                </div>
            </dialog>
        );
    }
}
