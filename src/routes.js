// @flow

import React from 'react';

import { Router, Route, Redirect, browserHistory } from 'react-router';
import { Provider } from 'react-redux';
import { syncHistoryWithStore } from 'react-router-redux';

import Bootstrap from './Layouts/Bootstrap';

import Library from './Pages/Library';
import Editor from './Pages/Editor';
import GlassList from './Pages/GlassList';
import Upload from './Pages/Upload';

import store from './store';

const history = syncHistoryWithStore(browserHistory, store);

const routes = (
    <Provider store={store}>
        <Router history={history}>
            <Route component={Bootstrap}>
                <Route path="/" component={Library} />
                <Route path="/edit/:projectId" component={Editor} />
                <Route path="/glass-list/:projectId" component={GlassList} />
                <Route path="/replace/:projectId" component={Upload} />
                <Route path="/add" component={Upload} />
            </Route>
            <Redirect from="*" to="/" />
        </Router>
    </Provider>
);

export default routes;
