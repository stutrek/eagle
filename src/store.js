// core imports
import { createStore, applyMiddleware } from 'redux';
import { browserHistory } from 'react-router';
import { routerMiddleware } from 'react-router-redux';

import rootReducer from './reducer'
// middleware
import thunk from 'redux-thunk';
import keyboardMiddleware from 'src/middleware/keyboard';
import { createLogger } from 'redux-logger';

let store;

const createdRouterMiddleware = routerMiddleware(browserHistory);

if (!localStorage.getItem('logs')) {
    store = createStore(
        rootReducer,
        // initialState,
        applyMiddleware(
            thunk,
            createdRouterMiddleware,
            keyboardMiddleware,
        )
    );
} else {
    // compose allow store to use middleware, devTools and other utils
    store = createStore(
        rootReducer,
        // initialState,
        applyMiddleware(
            thunk,
            createdRouterMiddleware,
            keyboardMiddleware,
            createLogger(),
        )
    );
}

export default store;
