//@flow
import { Path, Layer } from 'paper';

import { hasFill, styleAttrs } from './util';


export default function splitShapesOnEmptyPaths (shapes: Array<Path>): Array<Path> {

	// once the shapes have been cut down by the shapes above them, we can
	// cut the shapes on paths.
	for (let i=0; i < shapes.length; i++) {
		let path = shapes[i];
		if (hasFill(path)) {
			continue;
		}
		console.log('I have a path');
		for (let j=0; j < i; j++) {
			const candidate = shapes[j];
			if (hasFill(candidate) && path.intersects(candidate)) {

				// this causes Paper to reset some part of state.
				// without it, paper will occasionally return empty on the divide.
				if (!window.dontHack) {
					(new Layer([candidate, path])).exportSVG();
				}

				let dividedShape = candidate.divide(path);

				// if it split into only one child it doesn't actually overlap.
				if (true || dividedShape.children) {
					let dividedShapes = dividedShape.children || [dividedShape];
					dividedShapes = dividedShapes.filter(s => s.segments.length);
					console.log('divided');
					dividedShapes.forEach(s => {
						styleAttrs.forEach(attr => {
							s[attr] = candidate[attr];
						});
					});

					if (dividedShapes.length) {
						shapes.splice(j, 1);
						shapes.splice(j, 0, ...dividedShapes);
						j += dividedShapes.length-1;
						i += dividedShapes.length-1;
					}
				}

				(new Layer([candidate, path])).exportSVG();
				// let subtractedPath = path.subtract(candidate);
				// let subtractedPaths = subtractedPath.children || [subtractedPath];
				// subtractedPaths = subtractedPaths.filter(s => s.segments.length);

				// shapes.splice(i, 1, ...subtractedPaths);
				// path = shapes[i];
			}
		}
	}

	return shapes;
}
