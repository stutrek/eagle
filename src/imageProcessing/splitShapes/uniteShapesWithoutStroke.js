//@flow

import { Path } from 'paper';
import { hasFill } from './util';

export default function uniteShapesWithoutStroke (shapes: Array<Path>): Array<Path> {
	for (let i=shapes.length-1; i >= 0; i--) {
		let shape = shapes[i];
		if (hasFill(shape) && shape.hasStroke() === false) {
			let shapesToMerge = [];
			for (let j=0; j < i; j++) {
				const candidate = shapes[j];
				if (
					hasFill(candidate) &&
					candidate.fillColor.equals(shape.fillColor) &&
					shape.intersects(candidate)
				) {
					shapesToMerge.push(candidate);
					shapes.splice(j, 1);
					j--;
					i--;
				}
			}
			if (shapesToMerge.length) {
				const merged = shapesToMerge.reduce((s, toMerge) => toMerge.unite(shape), shape);
				shapes.splice(i, 1);
				shapes.unshift(merged);
				i++;
			}
		}
	}
	return shapes;
}
