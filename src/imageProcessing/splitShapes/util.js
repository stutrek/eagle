//@flow

import { Item, Path, Raster } from 'paper';

interface FlattenState {
	items: Array<Path>,
	clipPath: Path | null
}

function flattener (group: Item, state: FlattenState={items: [], clipPath: null}): FlattenState {
	let i = 0;
	let nextState = state;
	if (group.clipped) {
		nextState = {
			...state,
			clipPath: group.children[0].toPath()
		};
		i = 1;
	}

	for (i; i < group.children.length; i++) {
		let child = group.children[i];
		if (child.children) {
			flattener(child, nextState);
		} else if (child instanceof Raster) {
			// noop
		} else {

			let newItem: Path = (child instanceof Path) ? child : child.toPath();
			if (nextState.clipPath) {
				newItem = newItem.intersect(nextState.clipPath, {insert: false});
				if (newItem.children) {
					flattener(newItem.children, nextState);
				} else if (newItem.segments.length) {
					state.items.push(newItem);
				}
			} else {
				state.items.push(newItem);
			}
		}
	}

	return state;
}

export function flatten (group: Item): Array<Path> {
	return flattener(group).items;
}

export function hasFill (candidate: Path) {
	return !!(candidate.fillColor && candidate.fillColor.getAlpha());
}

export const styleAttrs = [
	'blendMode',
	'dashArray',
	'dashOffset',
	'fillColor',
	'fillRule',
	'strokeCap',
	'strokeColor',
	'strokeJoin',
	'strokeScaling',
	'strokeWidth'
];
