//@flow

import { Path, Layer } from 'paper';
import { hasFill, styleAttrs } from './util';

export default function subtractShapesAbove (shapes: Array<Path>): Array<Path> {
	for (let i=0; i < shapes.length; i++) {
		let shape = shapes[i];
		for (let j=i+1; j < shapes.length; j++) {
			const candidate = shapes[j];
			if (hasFill(candidate) && shape.intersects(candidate)) {
				if ( // these two that overlap to make a larger shape. This is often done to make
					// something wrap around another thing, like a ring going from behind a person
					// to in front of them. These will be handled in an additional pass.
					candidate.fillColor.equals(shape.fillColor) && (candidate.hasStroke() === false)
				) {
					continue;
				} else {
					(new Layer([candidate, shape])).exportSVG();
					let subtractedShape = shape.subtract(candidate);
					let subtractedShapes = subtractedShape.children || [subtractedShape];
					subtractedShapes.forEach(child => {
						styleAttrs.forEach(attr => {
							child[attr] = shape[attr];
						});
					});
					shapes.splice(i, 1, ...subtractedShapes);
					shape = shapes[i];
					j += subtractedShapes.length-1;
				}
			}
		}
	}
	return shapes;
}
