//@flow
import { Project, Layer, Path } from 'paper';

import subtractShapesAbove from './subtractShapesAbove';
import uniteShapesWithoutStroke from './uniteShapesWithoutStroke';
import splitShapesOnEmptyPaths from './splitShapesOnEmptyPaths';
import { hasFill, flatten } from './util';

export default function splitShapes (svg: string): SVGElement {
	const project: Project = new Project();
	const layer: Layer = project.importSVG(svg);


	let shapes = flatten(layer);

	let largestShapeAllowed = layer.bounds.area * 0.90;
	shapes = shapes.filter(s => s.area < largestShapeAllowed);

	// some editors make two shapes, one for fill and one for stroke.
	// this removes that.
	shapes = shapes.reduce((acc, shape): Array<Path> => {
		if (acc.length === 0) {
			return [shape];
		}
		if (shape.pathData === acc[acc.length-1].pathData) {
			if (hasFill(shape)) {
				acc[acc.length-1] = shape;
			} else {
				const oneToKeep = acc[acc.length-1];
				oneToKeep.strokeCap = shape.strokeCap;
				oneToKeep.strokeColor = shape.strokeColor;
				oneToKeep.strokeJoin = shape.strokeJoin;
				oneToKeep.strokeScaling = shape.strokeScaling;
				oneToKeep.strokeWidth = shape.strokeWidth;
			}
		} else {
			acc.push(shape);
		}
		return acc;
	}, []);

	// first we iterate over all the _shapes_ and subtract
	// the ones above from the one underneath.
	shapes = subtractShapesAbove(shapes);
	shapes = uniteShapesWithoutStroke(shapes);
	shapes = splitShapesOnEmptyPaths(shapes);

	shapes.forEach(shape => {
		shape.strokeColor = 'black';
		shape.strokeWidth = 2;
		shape.strokeJoin = 'round';
	});
	layer.remove();

	const project2: Project = new Project();
	const layer2 = new Layer(shapes);

	project2.addLayer(layer2);

	const returnElement: SVGElement = project2.exportSVG({bounds: 'content'});
	project2.clear();

	return returnElement;
}
