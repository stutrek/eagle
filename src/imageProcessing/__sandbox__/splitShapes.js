import React from 'react';

import splitShapes from '../splitShapes';

import { Layer } from 'paper';

import styles from './split-shapes-sandbox.module.css';

import svg from './three-shapes.svg';
// import svg from './lisa and pierre rectangle.svg';
// import svg from './overlapping with no stroke.svg';

window.putInDebug = function (shapes) {
	const layer = new Layer(shapes);
	const debugContainer = document.getElementById('debug-container');
	debugContainer.innerHTML = '';
	debugContainer.appendChild(layer.exportSVG());
};


export default class SplitShapes extends React.Component {
	setOriginal = (el) => {
		if (!el) {
			return;
		}

		el.innerHTML = svg;
	}

	setModified = el => {
		if (!el) {
			return;
		}

		let splitSVG = splitShapes(svg);
		console.log(splitSVG);
		el.appendChild(splitSVG);
		el.firstChild.style.width = '100%';
		el.firstChild.style.height = 'auto';
	}

	render () {
		return <div style={{maxWidth: '800px'}} className={styles.container}>
			{/* {/* <h2>Original</h2>
			<div style={{border: '1px solid black'}} ref={this.setOriginal} /> */}
			<h2>Modified</h2>
			<div style={{border: '1px solid black'}} ref={this.setModified} /> */}
			<h2>Debug</h2>
			<div style={{border: '1px solid black'}}>
				<svg id="debug-container" width="1300" height="800"></svg>
			</div>
		</div>;
	}
}
