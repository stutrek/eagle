import React from 'react';

import svg from './three-shapes.svg';

import coloredSVGtoWhite from '../coloredSVGtoWhite';

export default class CSVGTW extends React.Component {

	setOriginal = (el) => {
		if (!el) {
			return;
		}

		el.innerHTML = svg;
	}

	setModified = el => {
		if (!el) {
			return;
		}

		let whiteSVG = coloredSVGtoWhite(svg);
		console.log(whiteSVG);
		el.appendChild(whiteSVG);
	}

	render () {
		return <div>
			<h2>Original</h2>
			<div style={{border: '1px solid black'}} ref={this.setOriginal} />
			<h2>Modified</h2>
			<div style={{border: '1px solid black'}} ref={this.setModified} />
		</div>;
	}
}
