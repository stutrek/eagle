//@flow
import { Project } from 'paper';

export default function coloredSVGToWhite (svg: string): SVGElement {
	let project = new Project();
	project.importSVG(svg);

	// if it has no alpha the shapes behind it are likely important
	// if it's black it might be some sort of a traced outline.
	let coloredItems = project.getItems({
		recursive: true,
		match: (item) => {
			const color = item.fillColor;
			// if there is no fill this is an outline or something else
			// this algorithm doesn't carea about.
			if (item.hasFill() === false || !color || color.getAlpha() === 0) {
				return false;
			}

			// if it has no area we don't care about it.
			if (!item.area) {
				return false;
			}

			// this tries to catch outlined strokes.
			if (color.brightness === 0 && Math.abs(item.area) < (item.bounds.area / 10)) {
				return false;
			}

			return true;
		}
	});

	coloredItems.forEach(item => item.fillColor = 'white');

	const returnElement: SVGElement = project.exportSVG({bounds: 'content'});
	project.clear();

	return returnElement;
}
