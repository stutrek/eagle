// @flow
import potrace from './potrace';
import { CompoundPath, Point } from 'paper';
import uuid from 'uuid';
import tinycolor from 'tinycolor2';

import type { Piece } from 'src/Models';

import polylabel from './polylabel';
import { checkForSharpConcaveAngles } from './checkCurve';

export async function runPotrace(canvas: HTMLCanvasElement): Promise<Array<CompoundPath>> {
    let invertedCanvas = document.createElement('canvas');
    invertedCanvas.width = parseInt(canvas.width);
    invertedCanvas.height = parseInt(canvas.height);

    // document.body.appendChild(invertedCanvas);

    let invertedCtx = invertedCanvas.getContext('2d');
    invertedCtx.drawImage(canvas, 0, 0);

    invertedCtx.fillStyle = 'white';
    invertedCtx.globalCompositeOperation = 'difference';
    invertedCtx.fillRect(0, 0, canvas.width, canvas.height);

    potrace.clear();
    potrace.setCanvas(invertedCanvas);
    potrace.setParameter({
        optcurve: true,
        opttolerance: 0.01,
    });

    return new Promise(resolve =>
        potrace.process(() => {
            const pathStrings = potrace.getSVGPaths(1);
            const pathlist = potrace.getPathlist();

            const shapes: Array<CompoundPath> = pathStrings.reduce((acc, path, i) => {
                let firstPoint = pathlist[i].curve.c[pathlist[i].curve.c.length - 1];
                let parentPath = acc.find(p => p.contains(new Point(firstPoint.x, firstPoint.y)));
                if (parentPath) {
                    parentPath.importSVG(`<path d="${path}" />`);
                } else {
                    const curvePath = new CompoundPath(path);
                    acc.push(curvePath);
                }
                return acc;
            }, []);
            shapes.forEach(s => s.reverse());
            let firstShape = shapes[0];

            let totalArea = parseInt(canvas.width) * parseInt(canvas.height);

            // if the first piece takes up 80% of the area, it's a sun catcher
            // and that piece is the outline on the paper
            let error = totalArea / 80;
            if (Math.abs(totalArea - firstShape.firstChild.area) < error) {
                shapes.shift();
            }
            resolve(shapes);
        })
    );
}

const getColor = (context: CanvasRenderingContext2D, center) => {
    let x = Math.round(center.x);
    let y = Math.round(center.y);
    let [r, g, b] = context.getImageData(x, y, 1, 1).data;
    let color = { r, g, b };
    return tinycolor(color).toHexString();
};

export default async function createPieces(
    canvas: HTMLCanvasElement,
    image: HTMLImageElement,
    upload: any
) {
    const paths = await runPotrace(canvas);

    paths.forEach(path => path.scale(1 / upload.scale, [0, 0]));

    let colorContext;
    if (upload.type === 'image/svg+xml') {
        const colorCanvas = document.createElement('canvas');
        colorCanvas.width = canvas.width;
        colorCanvas.height = canvas.height;

        colorContext = colorCanvas.getContext('2d');
        colorContext.fillStyle = 'white';
        colorContext.fillRect(0, 0, upload.width, upload.height);
        colorContext.drawImage(image, 0, 0, upload.width, upload.height);
    }

    const pieces = paths.map((path, i) => {
        const center = polylabel(path);
        const nearestPointToLabel = path.getNearestPoint(center);
        const labelSize = nearestPointToLabel.getDistance(center);
        const bounds = path.bounds;

        let warnings = {
            concave: checkForSharpConcaveAngles(path),
        };

        let piece: Piece = {
            id: uuid(),
            d: path.getPathData(),
            glass: null,
            defaultColor: colorContext ? getColor(colorContext, center) : null,
            label: `${i + 1}`,
            labelCenter: center,
            labelSize: labelSize,
            note: '',
            title: '',
            area: path.area,
            warnings,
            bounds: {
                x: bounds.x,
                y: bounds.y,
                width: bounds.width,
                height: bounds.height,
            },
        };
        return piece;
    });
    return pieces;
}
