// @flow

import { CompoundPath, Path } from 'paper';
import type { Point } from 'src/Models';

function addPoints(a: Point, b: Point): Point {
	return {x: a.x+b.x, y: a.y+b.y};
}

function multiplyPoint(a: Point, n: number): Point {
	return {x: a.x*n, y: a.y*n};
}

export const checkForSharpConcaveAngles = (shape: CompoundPath): boolean => {
	let path = shape.firstChild;
	let firstNormal = path.getNormalAt(0);
	let firstPoint = path.getPointAt(0);
	let previousNormalPath = new Path([firstPoint, addPoints(firstPoint, multiplyPoint(firstNormal, 7))]);


	const increment = 5;
	for (var i=1; i < (path.length - increment) / increment; i++) {
		let normal = path.getNormalAt(i * increment);
		let point = path.getPointAt(i * increment);
		let normalPath = new Path([point, addPoints(point, multiplyPoint(normal, 7))]);
		if (normalPath.intersects(previousNormalPath)) {
			// shape.strokeColor = 'black';
			// shape.strokeWidth = 1;
			// normalPath.strokeColor = 'red';
			// previousNormalPath.strokeColor = 'red';
			// normalPath.strokeWidth = 3;
			// previousNormalPath.strokeWidth = 3;
			// paper.view.draw();
			// console.log(true);
			normalPath.remove();
			previousNormalPath.remove();
			return true;
		}
		previousNormalPath.remove();
		previousNormalPath = normalPath;

	}
	previousNormalPath.remove();
	//console.log(false);
	return false;
};
