// @flow

import {parseSVG, makeAbsolute} from 'svg-path-parser';
import polylabel from '@mapbox/polylabel';

import { CompoundPath } from 'paper';
import type { Point } from 'src/Models';

export default (shape: CompoundPath): Point => {
	let flattened : CompoundPath  = shape.clone();
	shape.flatten(1);
	let flatString : String = flattened.getPathData();
	let segmentStrings = flatString.split('M').filter(s => s).map(s => 'M'+s);

	let segments = segmentStrings.map(segment => {
		let commands : [{x: number, y: number}] = makeAbsolute(parseSVG(segment));
		let points = commands.map(p => [p.x, p.y]);
		return points;
	});
	let averages : [number, number] = polylabel(segments);

	return {
		x: averages[0],
		y: averages[1]
	};
};
