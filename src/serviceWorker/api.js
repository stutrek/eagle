import { getAllProjects, saveProject, getProject } from '../Models/db';

import { Router } from 'service-worker-router';

const router = new Router();

function createError(code, statusText, message) {
    const init = { status: code, statusText: statusText, type: 'application/json' };
    const response = new Response(JSON.stringify({ message }), init);
    return response;
}

function createSuccess(data) {
    const init = { status: 200, statusText: 'OK', type: 'application/json' };
    const response = new Response(JSON.stringify(data), init);
    return response;
}

async function getAll() {
    const data = await getAllProjects();
    return createSuccess(data);
}

async function getSingleProject({ params }) {
    const data = getProject(params.id);
    return createSuccess(data);
}

async function updateProject({ request, params }) {
    const data = await request.json();
    if (params.id !== data.id) {
        return createError(400, 'Bad Request', 'id in URL does not match project id.');
    }
    await saveProject(data);
    return createSuccess(data);
}

router.get('/api/projects', getAll);

router.get('/api/project/:id', getSingleProject);
router.post('/api/project/:id', updateProject);

export default async event => {
    router.handleEvent(event);
};
