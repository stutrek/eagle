// @flow weak
/* eslint-disable no-console */
import { CACHE_NAME } from './constants';
import fileCache from './fileCache';
import api from './api';

const { assets } = global.serviceWorkerOption;

const fileTypesToRemove = {
    eot: true,
    woff: true,
    ttf: true,
};

let assetsToCache = [...assets, './'].filter(item => {
    const ending = item.replace(/.*\./, '');
    return !fileTypesToRemove[ending];
});

assetsToCache = assetsToCache.map(path => {
    return new URL(path, global.location).toString();
});

// When the service worker is first added to a computer.
self.addEventListener('install', event => {
    // Add core website files to cache during serviceworker installation.
    event.waitUntil(
        global.caches
            .open(CACHE_NAME)
            .then(cache => {
                return cache.addAll(assetsToCache);
            })
            .then(() => {
                return self.skipWaiting();
            })
            .catch(error => {
                console.error(error);
                throw error;
            })
    );
});

// After the install event.
self.addEventListener('activate', event => {
    // Clean the caches
    event.waitUntil(
        global.caches.keys().then(async cacheNames => {
            await Promise.all(
                cacheNames.map(cacheName => {
                    // Delete the caches that are not the current one.
                    if (cacheName.indexOf(CACHE_NAME) === 0) {
                        return null;
                    }

                    return global.caches.delete(cacheName);
                })
            );
            return self.clients.claim();
        })
    );
});

self.addEventListener('message', event => {
    switch (event.data.action) {
        case 'skipWaiting':
            if (self.skipWaiting) {
                self.skipWaiting();
                self.clients.claim();
            }
            break;
        default:
            break;
    }
});

self.addEventListener('fetch', event => {
    const request = event.request;
    const requestUrl = new URL(request.url);

    // Ignore non GET requests.
    switch (true) {
        case requestUrl.pathname.startsWith('/api/'):
            api(event);
            break;

        case request.method !== 'GET':
        case request.mode === 'navigate':
        case requestUrl.origin !== location.origin:
            return;

        default:
            event.respondWith(fileCache(request));
    }
});
