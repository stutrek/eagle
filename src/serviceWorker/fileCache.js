import { CACHE_NAME } from './constants';

export default async request => {
    const cachedResponse = await global.caches.match(request);
    if (cachedResponse) {
        return cachedResponse;
    }

    const response = await fetch(request);
    if (!response || !response.ok) {
        return response;
    }

    try {
        const cache = await global.caches.open(CACHE_NAME);
        cache.put(request, response.clone());
    } catch (e) {
        console.error(e);
    }
    return response;
};
