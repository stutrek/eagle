// @flow

import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import paper from 'paper';

let paperCanvas = document.createElement('canvas');
paperCanvas.width = 1500;
paperCanvas.height = 2000;
paperCanvas.style.position = 'absolute';
paperCanvas.style.top = '0';
paperCanvas.style.left = '0';
paperCanvas.style.pointerEvents = 'none';
paperCanvas.style.maxWidth = '100vw';
paperCanvas.style.maxHeight = '100vh';
paper.setup(paperCanvas);
if (document.body) {
    document.body.appendChild(paperCanvas);
}

import 'semantic-ui-css/semantic.css';
import './styles/app.module.css';
import './styles/vars.module.css';
import './styles/button.module.css';

import InternalLinkRouter from 'internal-link-router';

import store from './store';
import routes from './routes';

const container = document.getElementById('app-container');

if (container) {
    ReactDOM.render(
        <Provider store={store}>
            <InternalLinkRouter>{routes}</InternalLinkRouter>
        </Provider>,
        container
    );
} else {
    throw new Error('Unable to find container');
}
