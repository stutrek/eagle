import { altUp, altDown } from 'src/actions/keyboard';

const keyboardMiddleware = store => next => {
	let altIsDown = false;

	const setAltUp = () => {
		if (altIsDown) {
			altIsDown = false;
			store.dispatch(altUp());
		}
	};
	const setAltDown = () => {
		if (!altIsDown) {
			altIsDown = true;
			store.dispatch(altDown());
		}
	}
	window.addEventListener('keydown', (event) => {
		if (event.key === 'Alt') {
			setAltDown();
		}
	});
	window.addEventListener('keyup', (event) => {
		if (event.key === 'Alt') {
			setAltUp();
		}
	});

	window.addEventListener('mousemove', event => {
		if (event.altKey !== altIsDown) {
			if (event.altKey) {
				setAltDown();
			} else {
				setAltUp();
			}
		}
	})

	return action => {
		return next(action)
	};
};

export default keyboardMiddleware;
