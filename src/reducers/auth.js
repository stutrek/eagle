import { LOGIN_SUCCESSFUL, SHOW_LOGIN, HIDE_LOGIN } from '../actions/auth';

const initialState = {
    dialogShowing: false,
    user: null,
};

export default (state = initialState, action) => {
    switch (action.type) {
        case LOGIN_SUCCESSFUL:
            state = {
                ...state,
                user: action.payload.user,
            };
            break;

        case SHOW_LOGIN:
            state = {
                ...state,
                dialogShowing: true,
            };
            break;

        case HIDE_LOGIN:
            state = {
                ...state,
                dialogShowing: false,
            };
            break;
    }

    return state;
};
