// @flow

import { UPDATE_PROJECT } from 'src/Components/Project/actions';
import { ADD_PROJECT } from 'src/Pages/Upload/actions';

import type { Project } from 'src/Models';
import { BOOTSTRAP_COMPLETE } from '../actions/bootstrap';

const defaultState: Array<Project> = [];

export default (state: Array<Project>=defaultState, action: any) => {
	switch (action.type) {
		case BOOTSTRAP_COMPLETE:
			state = action.payload.projects;
			break;

		case ADD_PROJECT:
			state = [...state, action.payload];
			break;

		case UPDATE_PROJECT:
			state = state.map(p => p.id === action.payload.id ? action.payload : p);

	}
	return state;
};
