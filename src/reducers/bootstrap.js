// @flow
import { BOOTSTRAP_START, BOOTSTRAP_COMPLETE } from 'src/actions/bootstrap';

type BootstrapState = {
	inProgress: boolean,
	complete: boolean
};

const defaultState: BootstrapState = {
	inProgress: false,
	complete: false
};

export default (state: BootstrapState=defaultState, action: any) => {
	switch (action.type) {
		case BOOTSTRAP_START:
			return {
				...defaultState,
				inProgress: true
			};

		case BOOTSTRAP_COMPLETE:
			return {
				...defaultState,
				complete: true
			};
	}
	return state;
};
