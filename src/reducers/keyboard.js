import { ALT_UP, ALT_DOWN } from 'src/actions/keyboard';

const initialState = {
	altDown: false
};

export default (state=initialState, action) => {
	switch (action.type) {
		case ALT_UP:
			return {
				...initialState,
				altDown: false
			};

		case ALT_DOWN:
			return {
				...initialState,
				altDown: true
			};
	}
	return state;
};
