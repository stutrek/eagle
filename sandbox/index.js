import React from 'react';
import ReactDOM from 'react-dom';

import { Route, Router, browserHistory } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';
import { Provider } from 'react-redux';

var req = require.context('src', true, /__sandbox__\/[A-Za-z].+\.js$/);

let fullPaths = {};

var pages = req
    .keys()
    .map(function(key) {
        let path = key
            .replace('/__sandbox__', '')
            .replace('./', '')
            .replace('.js', '');
        // var path = key.replace('./', '').replace('.js', '');
        var Component = req(key).default;
        fullPaths[path] = key;
        return {
            path,
            Component,
        };
    })
    .filter(a => a);

// import InternalLinkRouter from 'app/Components/InternalLinkRouter';
// import Singletons from 'Singletons';

import store from 'src/store';

import 'src/styles/vars.module.css';
import 'src/styles/app.module.css';

import './sandbox.module.css';
import './markdown.module.css';

// import readme from './README.md';

const history = syncHistoryWithStore(browserHistory, store);

class Welcome extends React.Component {
    render() {
        return <div dangerouslySetInnerHTML={{ __html: 'hi' }} />;
    }
}

class Wrapper extends React.Component {
    state = {
        hasError: false,
    };

    componentDidCatch(error, info) {
        this.setState({ hasError: true });
        console.error(error);
    }

    render() {
        let previousFolder = null;
        const path = this.props.location.pathname.substr(1);
        return (
            <div className="sandbox-container">
                <nav className="sandbox-nav">
                    {pages.reduce((acc, p) => {
                        let folder = /^([^/]+\/)+/.exec(p.path)[0];
                        folder = folder.substr(0, folder.length - 1);
                        if (folder !== previousFolder) {
                            acc.push(
                                <div key={folder} className="separator">
                                    {folder}
                                </div>
                            );
                            previousFolder = folder;
                        }
                        acc.push(
                            <a key={p.path} href={`/${p.path}`}>
                                {p.path.replace(folder, '')}
                            </a>
                        );
                        return acc;
                    }, [])}
                </nav>
                <div className="sandbox-path-header" key="1">
                    <h1>{path}</h1>
                </div>
                <div className="sandbox-content">
                    {this.state.hasError ? 'open console to see error' : this.props.children}
                </div>
            </div>
        );
    }
}

class ComponentTester extends React.Component {
    render() {
        return (
            <Router history={history}>
                <Route component={Wrapper}>
                    {pages.map(p => (
                        <Route
                            key={p.path}
                            path={`/${p.path}`}
                            fullPath={p.fullPath}
                            component={p.Component}
                        />
                    ))}
                    <Route path="/*" component={Welcome} />
                </Route>
            </Router>
        );
    }
}

var container = document.getElementById('app-container');

function startApp() {
    ReactDOM.render(
        <Provider store={store}>
            <ComponentTester />
        </Provider>,
        container
    );
}

startApp();
