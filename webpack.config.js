const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackInlineSVGPlugin = require('html-webpack-inline-svg-plugin');
const ForceCaseSensitivityPlugin = require('case-sensitive-paths-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ServiceWorkerWebpackPlugin = require('serviceworker-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

var IS_DEV;
if (typeof process.env.IS_DEV !== 'undefined') {
    IS_DEV = process.env.IS_DEV === 'true';
} else if (process.argv.indexOf('dev') !== -1) {
    IS_DEV = true;
} else {
    IS_DEV = process.argv.join('').indexOf('simple-dev-server') !== -1;
}

var isProfile = process.argv.join('').indexOf('--profile') !== -1;
const appConfig = { dev: {}, prod: {} };
if (!isProfile) {
    console.log('IS DEV?', IS_DEV);
}

var plugins = [
    new ForceCaseSensitivityPlugin(),

    new HtmlWebpackPlugin({
        template: __dirname + '/src/index.html',
        filename: 'index-prod.html',
        appConfig: appConfig.prod,
        chunks: ['app'],
    }),
    new HtmlWebpackPlugin({
        template: __dirname + '/src/index.html',
        filename: 'index-dev.html',
        appConfig: appConfig.dev,
        chunks: ['app'],
    }),
    new HtmlWebpackInlineSVGPlugin({ runPreEmit: true }),

    new CopyWebpackPlugin([{ from: 'robots.txt' }]),

    new ServiceWorkerWebpackPlugin({
        entry: __dirname + '/src/serviceWorker/index.js',
    }),

    new MiniCssExtractPlugin({
        filename: 'eagle-[id]-[hash].css',
    }),
];

var entry, output;

entry = {
    app: __dirname + '/src/index.js',
    // imageProcessor: __dirname + '/src/imageProcessor/index.js',
};

if (IS_DEV) {
    plugins.push(
        new HtmlWebpackPlugin({
            template: __dirname + '/sandbox/index.html',
            filename: 'sandbox.html',
            appConfig: appConfig.dev,
            chunks: ['sandbox'],
        })
    );
    entry.sandbox = __dirname + '/sandbox/index.js';
}

output = {
    path: __dirname + '/build',
    filename: 'assets/[name]-[hash].js',
    chunkFilename: 'assets/chunks/[id]-[hash].js',
    publicPath: '/',
};

var config = {
    entry: entry,

    output: output,

    plugins: plugins,

    mode: IS_DEV ? 'development' : 'production',

    optimization: {
        splitChunks: { minChunks: 2 },
    },

    module: {
        rules: [
            {
                test: /\.m?(js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                },
            },
            {
                test: /\.css$/,
                exclude: /\.module.css$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    {
                        loader: 'css-loader',
                        options: {
                            sourceMap: true,
                            modules: false,
                        },
                    },
                    {
                        loader: 'postcss-loader',
                        options: {
                            sourceMap: true,
                        },
                    },
                ],
            },
            {
                test: /\.module.css$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    {
                        loader: 'css-loader',
                        options: {
                            sourceMap: true,
                            modules: true,
                            camelCase: true,
                            importLoaders: 1,
                            localIdentName: '[name]_[local]',
                        },
                    },
                    {
                        loader: 'postcss-loader',
                        options: {
                            sourceMap: true,
                        },
                    },
                ],
            },
            {
                test: /\.(otf|eot|ttf|woff|woff2)$/,
                use: {
                    loader: 'file-loader',
                    options: {
                        limit: 1024,
                        name: 'assets/fonts/[name]-[hash].[ext]',
                    },
                },
            },
            {
                test: /\.(png|jpg|gif|ico)$/,
                use: {
                    loader: 'url-loader',
                    options: {
                        limit: 12288,
                        name: 'assets/images/[name]-[hash].[ext]',
                    },
                },
            },
            {
                test: /\.svg$/,
                issuer: issuer => issuer.replace(/.*\./g, '') !== 'css',
                use: 'svg-inline-loader',
            },
            {
                test: /\.svg$/,
                issuer: issuer => issuer.replace(/.*\./g, '') === 'css',
                use: {
                    loader: 'file-loader',
                    options: {
                        limit: 1024,
                        name: 'assets/fonts/[name]-[hash].[ext]',
                    },
                },
            },
            {
                test: /\.md$/,
                use: ['html-loader', 'markdown-loader'],
            },
        ],
    },

    devtool: 'source-map',

    resolve: {
        modules: [__dirname, __dirname + '/node_modules'],
    },
};

module.exports = config;
